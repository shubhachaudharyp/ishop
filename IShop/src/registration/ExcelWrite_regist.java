package registration;
import registration.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWrite_regist {

    private static final String FILE_NAME = "D:\\MCA\\Excel\\hello2.xlsx";
    int rowNum = 1;
    int colNum = 1;
    int getLasrRow = 1;
    HorizontalAlignment halign;
    VerticalAlignment valign;
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    CellStyle style;
    Font font;
    FileOutputStream outputStream;
    File obj_filePata;
    XSSFCellStyle my_style;
    Row row;
    org.apache.poi.ss.usermodel.Cell cell;
    CellStyle style2;
    XSSFFont font2;
    File src;
    FileInputStream fis;
    int widthInChars0 = 800;
    int widthInChars1 = 7000;
    int widthInChars2 = 2000;
    int widthInChars3 = 7000;
    int widthInChars4 = 15000;
    int widthInChars5 = 10000;

    public ExcelWrite_regist() {
    }

    public void checkFile() {
        src = new File(FILE_NAME);
        if (!src.exists()) {
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet();
        } else {
            try {
                fis = new FileInputStream(src);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ExcelWrite_regist.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                workbook = new XSSFWorkbook(fis);
            } catch (IOException ex) {
                Logger.getLogger(ExcelWrite_regist.class.getName()).log(Level.SEVERE, null, ex);
            }
            sheet = workbook.getSheetAt(0);
        }
        style = workbook.createCellStyle();
        font = workbook.createFont();
        setDegine();
    }

    public void setDegine() {

        my_style = workbook.createCellStyle();
        my_style.setBorderBottom(BorderStyle.MEDIUM);
        my_style.setBorderLeft(BorderStyle.MEDIUM);
        my_style.setBorderRight(BorderStyle.MEDIUM);
        my_style.setBorderTop(BorderStyle.MEDIUM);
        style2 = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(halign.CENTER);
        font2 = workbook.createFont();
    }

    void write(String sName, String sAge, String sPhone, String sAddress, String sEmail, String sPassword) {
        System.out.println("Creating Excel");
        row = sheet.createRow(rowNum++);
        cell = row.createCell(colNum++);

        font.setColor(IndexedColors.WHITE.getIndex());
        style.setFont(font);
        style2.setFont(font2);
        sheet.setColumnWidth(0, widthInChars0);
        sheet.setColumnWidth(1, widthInChars1);
        sheet.setColumnWidth(2, widthInChars2);
        sheet.setColumnWidth(3, widthInChars3);
        sheet.setColumnWidth(4, widthInChars4);
        sheet.setColumnWidth(5, widthInChars5);
        sheet.setColumnWidth(6, widthInChars3);
        font2.setBold(true);
        sheet.setDisplayGridlines(false);
        font2.setFontHeightInPoints((short) 20);
        getLasrRow = sheet.getLastRowNum();
        System.out.println(getLasrRow);
        //if (getLasrRow + 1 == colNum) {

        cell.setCellValue("User Information");
        font2.setUnderline(XSSFFont.U_SINGLE);
        cell.setCellStyle(style2);
        rowNum = 2;
        colNum = 1;

        row = sheet.createRow(rowNum + 1);
        cell = row.createCell(colNum++);
        cell.setCellValue("Name");
//        cell.setCellStyle(my_style);
        cell.setCellStyle(style);
        cell = row.createCell(colNum++);
        cell.setCellValue("Age");
        cell.setCellStyle(style);
        cell = row.createCell(colNum++);
        cell.setCellValue("Phone");
        cell.setCellStyle(style);

        cell = row.createCell(colNum++);
        cell.setCellValue("Address");
        cell.setCellStyle(style);
        cell = row.createCell(colNum++);
        cell.setCellValue("Email");
        cell.setCellStyle(style);
        cell = row.createCell(colNum++);
        cell.setCellValue("Password");
        cell.setCellStyle(style);



        getLasrRow = sheet.getLastRowNum();
        colNum = 1;
        row = sheet.createRow(getLasrRow + 1);
        cell = row.createCell(colNum++);
        cell.setCellValue(sName);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);
        cell.setCellValue(sAge);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);
        cell.setCellValue(sPhone);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);

        cell.setCellValue(sAddress);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);
        cell.setCellValue(sEmail);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);
        cell.setCellValue(sPassword);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);
        try {
            outputStream = new FileOutputStream(FILE_NAME);
            //String FileOutputStream = FILE_NAME;
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }
}
