package model.manage.purchasemanage;
import model.manage.productmanage.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Purchase_ExcelWrite {

    private static final String FILE_NAME = "D:\\MCA\\Excel\\Product.xlsx";
   
    int rowNum = 1;
    int colNum = 1;
    int getLasrRow = 1;
    int getLasrRow2 = 1;
    HorizontalAlignment halign;
    VerticalAlignment valign;
    XSSFWorkbook workbook;
    XSSFSheet sheet;
    CellStyle style;
    Font font;
    FileOutputStream outputStream;
    File obj_filePata;
    XSSFCellStyle my_style;
    Row row;
    org.apache.poi.ss.usermodel.Cell cell;
    CellStyle style2;
    XSSFFont font2;
    File src;
    FileInputStream fis;
    int widthInChars0 = 800;
    int widthInChars1 = 2000;
    int widthInChars2 = 7000;
    int widthInChars3 = 3000;

    public void checkFile() throws FileNotFoundException, IOException {
        src = new File(FILE_NAME);
        if (!src.exists()) {
            workbook = new XSSFWorkbook();
            sheet = workbook.createSheet();
        } else {
            fis = new FileInputStream(src);
            workbook = new XSSFWorkbook(fis);
            sheet = workbook.getSheetAt(0);
        }
        style = workbook.createCellStyle();
        font = workbook.createFont();
        setDegine();
    }

    public void setDegine() throws FileNotFoundException, IOException {

        my_style = workbook.createCellStyle();
        my_style.setBorderBottom(BorderStyle.MEDIUM);
        my_style.setBorderLeft(BorderStyle.MEDIUM);
        my_style.setBorderRight(BorderStyle.MEDIUM);
        my_style.setBorderTop(BorderStyle.MEDIUM);
        style2 = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(halign.CENTER);
        font2 = workbook.createFont();
    }

    public void write(final String s_Quantity) {
        System.out.println("Creating Excel");
        row = sheet.createRow(rowNum++);
        cell = row.createCell(colNum++);

        font.setColor(IndexedColors.WHITE.getIndex());
        style.setFont(font);
        style2.setFont(font2);
        sheet.setColumnWidth(0, widthInChars0);
        sheet.setColumnWidth(1, widthInChars1);
        sheet.setColumnWidth(2, widthInChars2);
        sheet.setColumnWidth(3, widthInChars3);
        font2.setBold(true);
        sheet.setDisplayGridlines(false);
        font2.setFontHeightInPoints((short) 20);
        getLasrRow = sheet.getLastRowNum();
        System.out.println(getLasrRow);        

        cell.setCellValue("Product Information");
        font2.setUnderline(XSSFFont.U_SINGLE);
        cell.setCellStyle(style2);
        rowNum = 2;
        colNum = 1;

        row = sheet.createRow(rowNum + 1);
        cell = row.createCell(colNum++);
        cell.setCellValue("ID");
        cell.setCellStyle(my_style);
        cell.setCellStyle(style);
        cell = row.createCell(colNum++);
        cell.setCellValue("Product Name");
        cell.setCellStyle(style);
        cell = row.createCell(colNum++);
        cell.setCellValue("Price");
        cell.setCellStyle(style);
        
        cell = row.createCell(colNum++);
        cell.setCellValue("Quantity");
        cell.setCellStyle(style);
        
        //} else {
        getLasrRow = sheet.getLastRowNum();
        colNum = 3;       
        row = sheet.createRow(getLasrRow + 1);
        cell = row.createCell(colNum++);       
        
        cell = row.createCell(colNum++);
        cell.setCellValue(s_Quantity);
        cell.setCellStyle(my_style);
        cell = row.createCell(colNum++);   
        try {
            outputStream = new FileOutputStream(FILE_NAME);            
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }
}
